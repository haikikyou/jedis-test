#!/bin/sh
#
# Install Redis
#

REDIS_VERSION=2.6.2
yum -y install gcc tcl

if [ ! -f /usr/local/bin/redis-server ] ; then
  cd /usr/local/src
  curl -O https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/redis/redis-${REDIS_VERSION}.tar.gz
  tar xzf redis-${REDIS_VERSION}.tar.gz
  cd redis-${REDIS_VERSION}
  make & make install
fi
