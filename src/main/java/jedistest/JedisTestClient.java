package jedistest;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.configuration.BaseConfiguration;
import org.apache.commons.configuration.ConfigurationUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;
import redis.clients.jedis.exceptions.JedisConnectionException;

public class JedisTestClient {

  final String REDIS_HOST = "192.168.33.101";

  final Log LOG = LogFactory.getLog(JedisTestClient.class);

  public static void main(String[] args) {
    try {
      new JedisTestClient().start(args);
    } catch (InterruptedException e) {
      e.printStackTrace();
      System.exit(-1);
    }
  }

  int start(String[] params) throws InterruptedException {
    BaseConfiguration options = init(params);

    ConfigurationUtils.dump(options, System.out);
    System.out.println();

    JedisPoolConfig config = new JedisPoolConfig();
    config.setMaxWait(options.getLong("maxWait"));

    List<JedisShardInfo> jedisList = new ArrayList<JedisShardInfo>();
    JedisShardInfo shardInfo = new JedisShardInfo(options.getString("host"));
    shardInfo.setTimeout(options.getInt("timeout"));
    jedisList.add(shardInfo);

    final ShardedJedisPool pool = new ShardedJedisPool(config, jedisList);


    for (int count = 0, rounds = options.getInt("rounds"); count < rounds; count++) {
      Benchmark bench = new Benchmark("#all", new TaskFactory() {
        @Override
        public BenchTask create(Benchmark bench, BaseConfiguration options) {
          return new BenchTask(bench, options) {
            @Override
            public void run() {
              try {
                ShardedJedis jedis = pool.getResource();
                try {
                  jedis.set("hoge", "foo");
                  pool.returnResource(jedis);
                  benchMark.reportSuccess();
                } catch (JedisConnectionException je) {
                  
                  benchMark.reportError();
                  if (jedis != null) {
                    pool.returnBrokenResource(jedis);
                    jedis = null;
                  }
                  LOG.error(je.getMessage());
                }
              } catch (Exception e) {
                LOG.error(e.getMessage());
                benchMark.reportError();
              }
            }
          };
        }
      }, options);
      Thread t = new Thread(bench);
      t.start();
      t.join();
      System.out.println(bench.getReport());
    }

    return 0;
  }

  BaseConfiguration init(String[] params) {
    BaseConfiguration options = new BaseConfiguration();

    int i = 0;
    options.addProperty("rounds", (params.length > i) ? Integer.parseInt(params[i++]): 50000);
    options.addProperty("repeat", (params.length > i) ? Integer.parseInt(params[i++]): 100);
    options.addProperty("interval", (params.length > i) ? Integer.parseInt(params[i++]): 1000);
    options.addProperty("visitors", (params.length > i) ? Integer.parseInt(params[i++]): 100);
    options.addProperty("timeout", (params.length > i) ? Integer.parseInt(params[i++]): 2000);
    options.addProperty("maxWait", (params.length > i) ? Integer.parseInt(params[i++]): -1);
    options.addProperty("host", (params.length > i) ? params[i++]: REDIS_HOST);

    return options;
  }
  
  static abstract class BenchTask implements Runnable {
    Benchmark benchMark;
    BaseConfiguration options;
    BenchTask(Benchmark benchMark, BaseConfiguration options) {
      this.benchMark = benchMark;
      this.options = options;
    }
  }

  /**
   * Create a BenchTask.
   */
  static interface TaskFactory {
    BenchTask create(Benchmark bench, BaseConfiguration options);
  }

  /**
   * Benchmark has settings for bench mark tests.
   */
  static class Benchmark implements Runnable {
    final Log LOG = LogFactory.getLog(Benchmark.class);

    long startTimeMills = 0;
    long endTimeMills = 0;
    final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    String name;
    BenchTask task;
    AtomicInteger successCount = new AtomicInteger(0);
    AtomicInteger failedCount = new AtomicInteger(0);
    BaseConfiguration options;
    TaskFactory taskFactory;

    public Benchmark(String name, TaskFactory taskFactory, BaseConfiguration options) {
      this.name = name;
      this.taskFactory = taskFactory;
      this.options = options;
    }

    public void run() {
      startTimeMills = System.currentTimeMillis();
      ExecutorService threadPool = Executors.newCachedThreadPool();
      try {
        int visitors = options.getInt("visitors");
        int interval = options.getInt("interval");
        int repeatCount = options.getInt("repeat") * visitors;
        for (int count = 0; count < repeatCount; count++) {
          threadPool.execute(taskFactory.create(this, options));
          if ((count + 1) % visitors == 0) {
            Thread.sleep(interval);
          }
        }
        
      } catch (Exception e) {
        LOG.error(e.getMessage());
      } finally {
        threadPool.shutdown();
      }
      endTimeMills = System.currentTimeMillis();
    }

    public int reportSuccess() {
      return successCount.incrementAndGet();
    }

    public int reportError() {
      return failedCount.incrementAndGet();
    }

    public String getReport() {
      long totalAroundTime = endTimeMills - startTimeMills;
      return String.format(StringUtils.join(new String[] {
          "[%s]",
          "startTime = %d",
          "endTime = %d",
          "success = %d",
          "error = %d",
          "req/sec = %.2f"
      }, "\t"),
          name,
          startTimeMills,
          endTimeMills,
          successCount.get(),
          failedCount.get(),
          (totalAroundTime <= 0) ? 0: ((successCount.get() + failedCount.get()) / (double)totalAroundTime) * 1000
          );
    }
  }
}
