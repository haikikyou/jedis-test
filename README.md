# Jedis Test


## Usage

Start up a virtual machine.

```
$ cd vagrant
$ vagrant up
$ vagrant ssh
$ redis-server
```

Build the project.

```
$ gradle eclipse
$ gradle jar
```

Run tests.

```
$ sh bench.sh 500000 10000 1000 100 2000 5000 localhost
```
