#!/bin/sh
#
# Jedis Bench
#

if [ $# -lt 1 ]; then
  echo "usage: $(basename $0) <rounds> <repeat> <interval> <visitors> <timeout> <maxWait> <host>"
  exit 0
fi

CLASSPATH=.
for f in `ls ./build/libs/*.jar`; do
  CLASSPATH=$CLASSPATH:$f
done
export CLASSPATH

echo "=> bench start"
java -Xms2048m -Xmx8192m jedistest.JedisTestClient "$@"